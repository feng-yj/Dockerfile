## Thrift for docker

![pulls](https://img.shields.io/docker/pulls/huiyifyj/thrift?logo=docker&style=flat-square&label=pulls)

| tag | layers |
| -- | -- |
| [`0.13.0 (latest)`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/thrift/0.13.0/Dockerfile) | ![layers](https://img.shields.io/microbadger/layers/huiyifyj/thrift/1.13.0?logo=docker&style=flat-square) |
| [`0.10.0`](https://gitlab.com/huiyifyj/Dockerfile/-/blob/master/thrift/0.10.0/Dockerfile) | ![layers](https://img.shields.io/microbadger/layers/huiyifyj/thrift/1.10.0?logo=docker&style=flat-square) |

#### Install
```shell
docker pull huiyifyj/thrift
```

#### Usage
```shell
docker run -v "$PWD:/data" thrift:0.13.0 \
    thrift -out /data --gen \
    go:thrift_import=github.com/huiyifyj/thrift \
    /data/test.thrift
```
