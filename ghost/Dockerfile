FROM debian:bullseye-slim

# https://docs.ghost.org/faq/node-versions
# https://github.com/TryGhost/Ghost/blob/v4.15.0/package.json#L49
ENV NODE_VERSION 14.17.6
ENV GHOST_VERSION 4.15.0
ENV GOSU_VERSION 1.12
ENV PATH /usr/local/node/bin:$PATH

RUN set -eux; \
	buildDeps="ca-certificates curl xz-utils"; \
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	apt-get update && apt-get install -y --no-install-recommends $buildDeps; \
	# ---------------------------- Node.js BEGIN ---------------------------- #
	cd /usr/local && mkdir node; \
	curl -sSL "https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz" -o node.tar.xz; \
	tar -Jxvf node.tar.xz -C /usr/local/node --strip-components=1; \
	rm -rf node.tar.xz; \
	# smoke tests node & npm
	node -v && npm -v; \
	# ----------------------------- Node.js END ----------------------------- #
	# install gosu
	curl -sSL "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-$dpkgArch" -o /usr/local/bin/gosu; \
	chmod +x /usr/local/bin/gosu; \
	gosu --version && gosu nobody true; \
	# create 'node' user for ghost
	groupadd -r node && useradd -r -g node --shell /bin/bash --create-home node; \
	# ----------------------------- Ghost BEGIN ----------------------------- #
	npm i -g yarn ghost-cli; \
	mkdir /ghost && chown node:node /ghost && cd /ghost; \
	gosu node ghost install ${GHOST_VERSION} --db sqlite3 --no-prompt --no-stack --no-setup; \
	# add more additional configurations
	gosu node ghost config --url http://localhost:2368; \
	gosu node ghost config --db mysql --dbhost localhost --dbuser root --dbname ghost; \
	gosu node ghost config paths.contentPath /ghost/content; \
	gosu node ghost config --ip 0.0.0.0 --port 2368; \
	# make a config.json symlink for NODE_ENV=development (and sanity check that it's correct)
	gosu node ln -s /ghost/config.production.json /ghost/config.development.json; \
	readlink -f /ghost/config.development.json; \
	# ------------------------------ Ghost END ------------------------------ #
	# clean cache
	yarn cache clean && npm cache clean --force; \
	gosu node yarn cache clean && gosu node npm cache clean --force; \
	apt-get purge -y --auto-remove $buildDeps; \
	rm -rf /var/cache/apt/*; \
	rm -rf /var/lib/apt/lists/*; \
	rm -rf /tmp/*; \
	rm -rf /var/tmp/*

WORKDIR /ghost
VOLUME /ghost/content

COPY entrypoint.sh /usr/local/bin

ENTRYPOINT ["entrypoint.sh"]

EXPOSE 2368

CMD ["node", "current/index.js"]
