## PowerShell Core
> [PowerShell Core](https://github.com/PowerShell/PowerShell) (alias **pwsh**) docker image.

![pulls](https://img.shields.io/docker/pulls/huiyifyj/pwsh?logo=docker&style=flat-square&label=pulls)

#### Install
```shell
docker pull huiyifyj/pwsh
```

#### Usage
```shell
docker run -it huiyifyj/pwsh
```
